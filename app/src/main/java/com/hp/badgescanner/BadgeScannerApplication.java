package com.hp.badgescanner;

import android.app.Application;

import com.hp.badgescanner.logger.Log;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class BadgeScannerApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread thread, Throwable ex) {
				Log.e("FATAL EXCEPTION", ex);
				System.exit(1);
			}
		});

		Realm.setDefaultConfiguration(new RealmConfiguration.Builder(this).build());
	}
}
