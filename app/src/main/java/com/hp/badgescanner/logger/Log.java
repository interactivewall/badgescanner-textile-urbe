package com.hp.badgescanner.logger;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.realm.Realm;

public class Log {

    private static final Pattern CLASS_NAME_PATTERN =
            Pattern.compile("([A-Z]*|(^[a-z]))[_\\da-z\\$]*");

    private static final String UNKNOWN_SIGNATURE = "[unknown]";

    public static void v(String message) {
        try {
            String className = getCallerClassName();
            android.util.Log.d(className, message);
            logToRealm(className, getCallerMethodName(), LogModel.VERBOSE, message, null);
        } catch (Throwable e) {
            android.util.Log.e("Log", "Failed to write log", e);
        }
    }

    public static void d(String message) {
        try {
            String className = getCallerClassName();
            android.util.Log.d(className, message);
            logToRealm(className, getCallerMethodName(), LogModel.DEBUG, message, null);
        } catch (Throwable e) {
            android.util.Log.e("Log", "Failed to write log", e);
        }
    }

    public static void i(String message) {
        try {
            String className = getCallerClassName();
            android.util.Log.i(className, message);
            logToRealm(className, getCallerMethodName(), LogModel.INFORMATION, message, null);
        } catch (Throwable e) {
            android.util.Log.e("Log", "Failed to write log", e);
        }
    }

    public static void w(String message) {
        try {
            String className = getCallerClassName();
            android.util.Log.w(className, message);
            logToRealm(className, getCallerMethodName(), LogModel.WARNING, message, null);
        } catch (Throwable e) {
            android.util.Log.e("Log", "Failed to write log", e);
        }
    }

    public static void e(String message, Throwable e) {
        try {
            String className = getCallerClassName();
            android.util.Log.e(className, message, e);
            logToRealm(className, getCallerMethodName(), LogModel.ERROR, message, android.util.Log.getStackTraceString(e));
        } catch (Throwable throwable) {
            android.util.Log.e("Log", "Failed to write log", throwable);
        }
    }

    private static synchronized void logToRealm(
            String className,
            String methodName,
            int level,
            String message,
            String stackTrace) {
        Realm realm = Realm.getDefaultInstance();
        realm.setAutoRefresh(false);
        realm.beginTransaction();
        try {
            LogModel logModel = realm.createObject(LogModel.class);
            logModel.setId(UUID.randomUUID().toString());
            logModel.setDate(new Date(System.currentTimeMillis()));
            logModel.setClassName(className);
            logModel.setMethodName(methodName);
            logModel.setLevel(level);
            logModel.setMessage(message);
            logModel.setStackTrace(stackTrace);
        } catch (Throwable t) {
            realm.cancelTransaction();
            throw t;
        } finally {
            realm.commitTransaction();
        }
        realm.close();
    }

    private static String tokenizeClassName(String className) {
        List<String> parts = new ArrayList<>();
        String result;

        try {
            Matcher matcher = CLASS_NAME_PATTERN.matcher(className);

            while (matcher.find()) {
                String part = className.substring(matcher.start(), matcher.end());
                if (!TextUtils.isEmpty(part.trim())) {
                    parts.add(part.toUpperCase());
                }
            }

            result = parts.isEmpty() ? className : TextUtils.join("_", parts);
        } catch (Exception e) {
            result = className;
        }

        return result;
    }

    private static String getCallerClassName() {
        String className;
        final int CALLER_STACK_INDEX = 4;

        try {
            StackTraceElement[] callStack = Thread.currentThread().getStackTrace();
            if (callStack.length > CALLER_STACK_INDEX) {
                String fullClassName = callStack[CALLER_STACK_INDEX].getClassName();
                int lastPoint = fullClassName.lastIndexOf(".");
                if (lastPoint >= 0 && lastPoint + 1 < fullClassName.length()) {
                    className = fullClassName.substring(lastPoint + 1);
                } else {
                    className = UNKNOWN_SIGNATURE;
                }
            } else {
                className = UNKNOWN_SIGNATURE;
            }
        } catch (Exception e) {
            className = UNKNOWN_SIGNATURE;
        }

        return tokenizeClassName(className);
    }

    private static String getCallerMethodName() {
        String methodName;
        final int CALLER_STACK_INDEX = 4;

        try {
            StackTraceElement[] callStack = Thread.currentThread().getStackTrace();
            if (callStack.length > CALLER_STACK_INDEX) {
                methodName = callStack[CALLER_STACK_INDEX].getMethodName();
            } else {
                methodName = UNKNOWN_SIGNATURE;
            }
        } catch (Exception e) {
            methodName = UNKNOWN_SIGNATURE;
        }

        return methodName;
    }
}
