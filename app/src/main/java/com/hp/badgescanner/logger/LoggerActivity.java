package com.hp.badgescanner.logger;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.badgescanner.R;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

public class LoggerActivity extends AppCompatActivity {

    private RealmResults<LogModel> results;
    private RecyclerView recyclerView;

    private final RealmChangeListener callback = new RealmChangeListener() {
        @Override
        public void onChange() {
            adapter.notifyDataSetChanged();
            recyclerView.scrollToPosition(results.size() - 1);
        }
    };

    private Adapter adapter;
    private Realm realm;

    public static void start(Context context) {
        Intent i = new Intent(context, LoggerActivity.class);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logger);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        realm = Realm.getDefaultInstance();

        results = realm.where(LogModel.class).findAllSortedAsync("date", Sort.ASCENDING);
        results.addChangeListener(callback);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        assert recyclerView != null;
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter = new Adapter(results, getLayoutInflater()));
    }

    @Override
    protected void onStop() {
        results.removeChangeListener(callback);
        realm.close();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logger, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear:
                new AlertDialog.Builder(this)
                        .setMessage("Are you sure that you want to delete all logs?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                realm.beginTransaction();
                                try {
                                    realm.clear(LogModel.class);
                                } catch (Exception e) {
                                    Log.e("Failed to clear logs", e);
                                    realm.cancelTransaction();
                                } finally {
                                    realm.commitTransaction();
                                }
                            }
                        }).setNegativeButton(android.R.string.no, null)
                        .show();
                return true;
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private static class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

        private final RealmResults<LogModel> realmResults;
        private final LayoutInflater inflater;

        private Adapter(RealmResults<LogModel> realmResults, LayoutInflater inflater) {
            this.realmResults = realmResults;
            this.inflater = inflater;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(inflater.inflate(R.layout.list_item_log, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            LogModel log = realmResults.get(position);
            holder.position = position;
            String message = log.getDate().toString() + " " + LogModel.getLevelName(log.getLevel()) + ": " + log.getMessage();
            if(log.getStackTrace() != null) {
                message += " " + log.getStackTrace();
            }
            holder.message.setText(message);
        }

        @Override
        public int getItemCount() {
            return realmResults.size();
        }

        static class ViewHolder extends RecyclerView.ViewHolder {
            int position;
            final TextView message;

            public ViewHolder(View itemView) {
                super(itemView);
                message = (TextView) itemView.findViewById(R.id.log_message);
            }
        }
    }
}
