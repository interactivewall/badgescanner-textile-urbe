package com.hp.badgescanner.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.hp.badgescanner.Prefs;
import com.hp.badgescanner.R;
import com.hp.badgescanner.logger.Log;

public class WebViewActivity extends AppCompatActivity {

    private static final String ID = "id";

    public static void start(Context context, String id) {
        Intent i = new Intent(context, WebViewActivity.class);
        i.putExtra(ID, id);
        context.startActivity(i);
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view);

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        WebView webView = (WebView) findViewById(R.id.content_web_view);
        assert webView != null;
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                switch (consoleMessage.messageLevel()) {
                    case DEBUG:
                        Log.d("WebViewConsole:" + consoleMessage.message());
                        break;
                    case ERROR:
                        Log.e("WebViewConsole:" + consoleMessage.message(), null);
                        break;
                    case LOG:
                        Log.v("WebViewConsole:" + consoleMessage.message());
                        break;
                    case WARNING:
                        Log.w("WebViewConsole:" + consoleMessage.message());
                        break;
                    case TIP:
                        Log.i("WebViewConsole:" + consoleMessage.message());
                        break;
                }
                return super.onConsoleMessage(consoleMessage);
            }
        });
        webView.getSettings().setSupportZoom(false);
        webView.getSettings().setBuiltInZoomControls(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            webView.getSettings().setDisplayZoomControls(false);
        }
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        webView.addJavascriptInterface(new JsInterface(), "_AttendeeWizardWebView");

        Uri contentUri = Uri.parse(Prefs.getWebViewUrl(this))
                .buildUpon()
                .appendQueryParameter("id", getIntent().getStringExtra(ID))
                .build();

        webView.loadUrl(contentUri.toString());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private class JsInterface {

        @JavascriptInterface
        public void navigateToScanning() {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            });
        }

    }
}
