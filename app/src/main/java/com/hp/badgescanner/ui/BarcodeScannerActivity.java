package com.hp.badgescanner.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.gson.GsonBuilder;
import com.hp.badgescanner.BuildConfig;
import com.hp.badgescanner.Prefs;
import com.hp.badgescanner.R;
import com.hp.badgescanner.custom.CustomScannerView;
import com.hp.badgescanner.logger.Log;
import com.hp.badgescanner.logger.LoggerActivity;
import com.hp.badgescanner.network.ApiService;
import com.hp.badgescanner.network.RequestModel;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import me.dm7.barcodescanner.zbar.BarcodeFormat;
import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@SuppressWarnings("deprecation")
public class BarcodeScannerActivity extends AppCompatActivity implements ZBarScannerView.ResultHandler, CameraSelectorDialogFragment.CameraSelectorDialogListener {

	private static final String FLASH_STATE = "FLASH_STATE";
	private static final String AUTO_FOCUS_STATE = "AUTO_FOCUS_STATE";
	private static final String CAMERA_ID = "CAMERA_ID";

	private CustomScannerView scannerView;
	private boolean flashEnabled;
	private boolean autoFocusEnabled;
	private int cameraId;

	private OkHttpClient okHttpClient;
	private ApiService apiService;

	@Override
	public void onCreate(Bundle state) {
		super.onCreate(state);
		if (state != null) {
			flashEnabled = state.getBoolean(FLASH_STATE, false);
			autoFocusEnabled = state.getBoolean(AUTO_FOCUS_STATE, true);
			cameraId = state.getInt(CAMERA_ID, 0);
		} else {
			flashEnabled = false;
			autoFocusEnabled = true;
			cameraId = 0;
			int numberOfCameras = Camera.getNumberOfCameras();
			Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
			for (int i = 0; i < numberOfCameras; i++) {
				Camera.getCameraInfo(i, cameraInfo);
				if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
					cameraId = i;
					break;
				}
			}
		}

		OkHttpClient.Builder builder = new OkHttpClient.Builder()
				.connectTimeout(1, TimeUnit.MINUTES)
				.writeTimeout(1, TimeUnit.MINUTES)
				.readTimeout(1, TimeUnit.MINUTES);
		if(BuildConfig.DEBUG) {
			try {
				HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
					@Override
					public void log(String message) {
						Log.v(message);
					}
				});
				interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
				builder.addNetworkInterceptor(interceptor);
			} catch (Throwable t) {
				Log.e("Failed to add HttpLoggingInterceptor", t);
			}
		}
		okHttpClient = builder.build();

		setContentView(R.layout.scanner);
		scannerView = (CustomScannerView) findViewById(R.id.scanner);
        scannerView.setFormats(Collections.singletonList(BarcodeFormat.QRCODE));
		scannerView.setResultHandler(this);
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayShowTitleEnabled(false);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		scannerView.startCamera(cameraId);
		scannerView.setFlash(flashEnabled);
		scannerView.setAutoFocus(autoFocusEnabled);

		String baseUrl = Prefs.getServerUrl(this);
		if(TextUtils.isEmpty(baseUrl)) {
			baseUrl = BuildConfig.SERVER_URL;
		}
		if(baseUrl.charAt(baseUrl.length() - 1) != '/') {
			baseUrl += "/";
		}

		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(baseUrl)
				.client(okHttpClient)
				.addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
				.build();
		apiService = retrofit.create(ApiService.class);
	}

	@Override
	public void onPause() {
		super.onPause();
		scannerView.stopCamera();
	}

	@Override
	public void handleResult(Result rawResult) {
		Log.d("Scanned id " + rawResult.getContents());
		if(scannerView.getLastCameraParameters() == null || scannerView.getLastFrameData() == null || TextUtils.isEmpty(rawResult.getContents())) {
			return;
		}

		final String id = rawResult.getContents();
		RequestModel requestModel = new RequestModel();
		requestModel.id = id;

		Camera.Parameters parameters = scannerView.getLastCameraParameters();
		Camera.Size size = parameters.getPreviewSize();
		YuvImage image = new YuvImage(scannerView.getLastFrameData(), parameters.getPreviewFormat(), size.width, size.height, null);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		image.compressToJpeg(new Rect(0, 0, image.getWidth(), image.getHeight()), 100, out);

		byte[] data = out.toByteArray();
		Bitmap storedBitmap = BitmapFactory.decodeByteArray(data, 0, data.length, null);
		Matrix mat = new Matrix();
		mat.postRotate(90);
		Bitmap rotated = Bitmap.createBitmap(storedBitmap, 0, 0, storedBitmap.getWidth(), storedBitmap.getHeight(), mat, true);
		if(storedBitmap != rotated) {
			storedBitmap.recycle();
		}
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		rotated.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
		data = byteArrayOutputStream.toByteArray();
		rotated.recycle();
		requestModel.badge = Base64.encodeToString(data, Base64.NO_WRAP);

		final ProgressDialog progressDialog = ProgressDialog.show(this, getString(R.string.app_name), "Sending data to server", false, false);
		apiService.addBadge(requestModel).enqueue(new Callback<Object>() {
			@Override
			public void onResponse(Call<Object> call, Response<Object> response) {
				progressDialog.dismiss();

				if(response.isSuccessful()) {
					if(BuildConfig.PROCEED_TO_WEB_VIEW) {
						scannerView.resumeCameraPreview(BarcodeScannerActivity.this);
						WebViewActivity.start(BarcodeScannerActivity.this, id);
					} else {
						new AlertDialog.Builder(BarcodeScannerActivity.this)
								.setMessage("Badge has been scanned and sent successfully")
								.setCancelable(false)
								.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										scannerView.resumeCameraPreview(BarcodeScannerActivity.this);
									}
								})
								.show();
					}
				} else {
					String errorMessage;
					int responseCode = response.code();
					if(responseCode == 500) {
						try {
							JSONObject jsonBody = new JSONObject(new String(response.errorBody().bytes()));
							errorMessage = jsonBody.getString("errorMsg");
						} catch (Exception e) {
							Log.e("Failed to parse error response from server", e);
							errorMessage = "Failed to send badge data. Error code " + responseCode + ".";
						}
					} else {
						errorMessage = "Failed to send badge data. "+ responseCode + " " + response.message();
					}
					Log.e(errorMessage, null);
					new AlertDialog.Builder(BarcodeScannerActivity.this)
							.setMessage(errorMessage)
							.setCancelable(false)
							.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									scannerView.resumeCameraPreview(BarcodeScannerActivity.this);
								}
							})
							.show();
				}
			}

			@Override
			public void onFailure(Call<Object> call, Throwable t) {
				Log.e("AddBadge request failed: " + t.getMessage(), t);
				progressDialog.dismiss();
				scannerView.resumeCameraPreview(BarcodeScannerActivity.this);
				new AlertDialog.Builder(BarcodeScannerActivity.this)
						.setMessage("Request to server has failed due to exception: " + t.getMessage())
						.setCancelable(false)
						.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								scannerView.resumeCameraPreview(BarcodeScannerActivity.this);
							}
						})
						.show();
			}
		});
	}

	@Override
	public void onCameraSelected(int newCameraId) {
		if (cameraId != newCameraId) {
			scannerView.stopCamera();
			cameraId = newCameraId;
			scannerView.startCamera(cameraId);
			scannerView.setFlash(flashEnabled && getCameraFacing(newCameraId) == Camera.CameraInfo.CAMERA_FACING_BACK);
			scannerView.setAutoFocus(autoFocusEnabled);
			invalidateOptionsMenu();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.scanner, menu);
		MenuItem flashMenuItem = menu.findItem(R.id.flash);
		flashMenuItem.setTitle(getCameraFacing(cameraId) == Camera.CameraInfo.CAMERA_FACING_BACK && flashEnabled ? R.string.menu_flash_on : R.string.menu_flash_off);
		flashMenuItem.setEnabled(getCameraFacing(cameraId) == Camera.CameraInfo.CAMERA_FACING_BACK);
		MenuItem focusMenuItem = menu.findItem(R.id.focus);
		focusMenuItem.setTitle(getCameraFacing(cameraId) == Camera.CameraInfo.CAMERA_FACING_BACK && autoFocusEnabled ? R.string.menu_focus_on : R.string.menu_focus_off);
		focusMenuItem.setEnabled(getCameraFacing(cameraId) == Camera.CameraInfo.CAMERA_FACING_BACK);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_settings:
				SettingsActivity.start(this);
				return true;
			case R.id.action_logs:
				LoggerActivity.start(this);
				return true;
			case R.id.flash:
				flashEnabled = !flashEnabled;
				invalidateOptionsMenu();
				scannerView.setFlash(flashEnabled);
				return true;
			case R.id.focus:
				autoFocusEnabled = !autoFocusEnabled;
				invalidateOptionsMenu();
				scannerView.setAutoFocus(autoFocusEnabled);
				return true;
			case R.id.camera:
				DialogFragment cFragment = CameraSelectorDialogFragment.newInstance(this, cameraId);
				cFragment.show(getSupportFragmentManager(), "camera_selector");
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean(FLASH_STATE, flashEnabled);
		outState.putBoolean(AUTO_FOCUS_STATE, autoFocusEnabled);
		outState.putInt(CAMERA_ID, cameraId);
	}

	private static int getCameraFacing(int cameraId) {
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		Camera.getCameraInfo(cameraId, cameraInfo);
		return cameraInfo.facing;
	}
}
