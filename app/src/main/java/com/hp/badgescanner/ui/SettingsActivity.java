package com.hp.badgescanner.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.hp.badgescanner.Prefs;
import com.hp.badgescanner.R;
import com.hp.badgescanner.logger.Log;

public class SettingsActivity extends AppCompatActivity {

	private EditText urlEditText;
	private EditText webViewUrlEditText;

	public static void start(Context context) {
		Intent i = new Intent(context, SettingsActivity.class);
		context.startActivity(i);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
		ActionBar actionBar = getSupportActionBar();
		if(actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
		urlEditText = (EditText) findViewById(R.id.url);
		urlEditText.setText(Prefs.getServerUrl(this));
		webViewUrlEditText = (EditText) findViewById(R.id.web_view_url);
		webViewUrlEditText.setText(Prefs.getWebViewUrl(this));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.settings, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_save:
				String serverUrl = urlEditText.getText().toString();
				String oldServerUrl = Prefs.getServerUrl(this);
				String webViewUrl = webViewUrlEditText.getText().toString();
				String oldWebViewUrl = Prefs.getWebViewUrl(this);
				Prefs.saveServerUrl(this, serverUrl);
				Prefs.saveWebViewUrl(this, webViewUrl);
				if(!oldServerUrl.equals(serverUrl)) {
					Log.i("Server url changed to " + serverUrl);
				}
				if(!oldWebViewUrl.equals(webViewUrl)) {
					Log.i("WebView url changed to " + webViewUrl);
				}
				finish();
				return true;
			case android.R.id.home:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
