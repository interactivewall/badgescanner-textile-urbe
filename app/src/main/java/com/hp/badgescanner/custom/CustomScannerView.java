package com.hp.badgescanner.custom;

import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;

import com.hp.badgescanner.logger.Log;

import me.dm7.barcodescanner.core.IViewFinder;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

@SuppressWarnings("deprecation")
public class CustomScannerView extends ZBarScannerView {

    private byte[] lastFrameData;
    private Camera.Parameters lastCameraParameters;

    public CustomScannerView(Context context) {
        super(context);
    }

    public CustomScannerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {

        try {
            lastCameraParameters = camera.getParameters();
            lastFrameData = data;
        } catch (Exception e) {
            Log.v("Failed to get camera parameters");
            lastCameraParameters = null;
            lastFrameData = null;
        }
        super.onPreviewFrame(data, camera);
    }

    public byte[] getLastFrameData() {
        return lastFrameData;
    }

    public Camera.Parameters getLastCameraParameters() {
        return lastCameraParameters;
    }

    @Override
    protected IViewFinder createViewFinderView(Context context) {
        return new EmptyFinderView(context);
    }
}
