package com.hp.badgescanner;

import android.content.Context;
import android.content.SharedPreferences;

public class Prefs {

	private static final String PREFS_FILE_NAME = "badge_scanner.prefs";
	private static final String PREF_URL = "url";
	private static final String PREF_WEB_VIEW_URL = "webViewUrl";

	public static void saveWebViewUrl(Context context, String url) {
		getEditor(context).putString(PREF_WEB_VIEW_URL, url).apply();
	}

	public static String getWebViewUrl(Context context) {
		return getPreferences(context).getString(PREF_WEB_VIEW_URL, BuildConfig.WEB_VIEW_URL);
	}

	public static void saveServerUrl(Context context, String url) {
		getEditor(context).putString(PREF_URL, url).apply();
	}

	public static String getServerUrl(Context context) {
		return getPreferences(context).getString(PREF_URL, BuildConfig.SERVER_URL);
	}

	private static SharedPreferences getPreferences(Context context) {
		return context.getSharedPreferences(PREFS_FILE_NAME, 0);
	}

	private static SharedPreferences.Editor getEditor(Context context) {
		return getPreferences(context).edit();
	}

	private Prefs() {}
}
