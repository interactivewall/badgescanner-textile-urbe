package com.hp.badgescanner.network;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiService {

    @POST("/api/addBadge")
    Call<Object> addBadge(@Body RequestModel requestModel);
}
